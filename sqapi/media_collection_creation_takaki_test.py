from mediadata import SQAPIMediaData
import requests
import json


def main():
    url = "http://soi.squidle.org"
    api_token = "29faefa1b8b7ae436f53f17e731388181c501a11da4f46dfb583220b"
    md = SQAPIMediaData(api_token=api_token, url=url)
    user_id = (
        requests.get(
            url + "/api/users",
            params={
                "q": json.dumps(
                    {
                        "filters": [
                            {"name": "api_token", "op": "eq", "val": api_token}
                        ],
                        "single": True,
                    }
                )
            },
        )
            .json()
            .get("id")
    )
    filters = [
        {
            "name": "key",
            "op": "in",
            "val": [
                "PCO_190916090521982683_FC.png ",
                "PCO_190916090846513415_FC.png ",
                "PCO_190916090955695546_FC.png ",
            ],
        },
        {"name": "deployment_id", "op": "eq", "val": 732},
    ]

    # filters = [
    #     # {
    #     #     "name": "key",
    #     #     # "name": "url",
    #     #     "op": "eq",
    #     #     "val": "PCO_190916090521982683_FC.png ",
    #     #     # "val": 4936806,
    #     # },
    #     {"name": "deployment_id", "op": "eq", "val": 732},
    # ]

    md.new_media_collection("test_dy109_test_01", user_id, filters)
    pass


if __name__ == "__main__":
    main()
