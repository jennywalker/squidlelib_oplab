import json
import requests


class SQAPIDirect():
    def __init__(self):
        self.api_token = 'a10528442ed2f82b598c69aa02e626addf2b0e6ed52a157fd3136460'
        self.url = "https://soi.squidle.org"

        # get user_id
        params = \
            {"q": json.dumps({"filters": [
                {"name": "api_token", "op": "eq", "val": self.api_token}],
                "single": True})}
        self.user_id = requests.get(
            self.url + "/api/users", params=params).json().get("id")

        self.headers = {
            "auth-token": self.api_token,
            "Content-type": "application/json",
            "Accept": "application/json",
        }

    def create_mc_test(self):
        filters = [
            {
                "name": "key",
                "op": "in",
                "val": [
                    "PCO_190916090521982683_FC.png ",
                    # "PCO_190916090846513415_FC.png ",
                    # "PCO_190916090955695546_FC.png ",
                ],
            },
            {"name": "deployment_id", "op": "eq", "val": "734"},
        ]

        payload = {
            "name": "API test 01",
            "description": "Testing API-created media_collection",
            "user_id": self.user_id
        }

        url_create = self.url + "/api/media_collection"

        r = requests.post(
            url_create,
            headers=self.headers,
            json=payload,
        )

        new_media_collection = r.json()
        media_collection_id = new_media_collection.get("id")

        patch_url = \
            self.url + \
            "/api/media_collection/" + \
            str(media_collection_id) \
            + "/media"

        query_json = json.dumps(
            {"filters": filters})  # , separators=(',', ':'))

        # patch_url += "?q=" + query_json
        payload2 = {
            "user_id": self.user_id,
            "q": query_json
        }

        r2 = requests.post(
            patch_url,
            headers=self.headers,
            json=payload2
        )
        print(r2.text)

        filters_additional = [
            {
                "name": "key",
                "op": "in",
                "val": [
                           # "PCO_190916090521982683_FC.png ",
                           "PCO_190916090846513415_FC.png ",
                           "PCO_190916090955695546_FC.png ",
                       ] * 1000,
            },
            {"name": "deployment_id", "op": "eq", "val": "734"},
        ]

        query_additional_json = json.dumps(
            {"filters": filters_additional})  # , separators=(',', ':'))

        # patch_url += "?q=" + query_json
        payload_additional = {
            "user_id": self.user_id,
            "q": query_additional_json
        }

        r3 = requests.patch(
            self.url + '/api/media_collection/' + str(media_collection_id),
            headers=self.headers,
            json=payload_additional
        )

        print(r3.text)
        return media_collection_id

    def create_mc(self, deployment_id, list_image_filename, mc_name,
                  description=None):
        # POST
        # /api/media_collection
        #
        # Create new media_collection object. Expects JSON. Fields shown in MODEL COLUMNS below.
        #
        # Preprocessors:
        # assert_user_login(): Restrict request to logged in users only

        # POST
        # /api/media_collection/<int:media_collection_id>/media
        #
        # Add media objects matching the search query defined in the json post parameter q to the media_collection with an ID matching media_collection_id. q is a search query in the format defined in api_query section and is sent as either a URL parameter or form post parameter. Request returns the corresponding media_collection.

        # create blank media collection at first.
        url_create = self.url + "/api/media_collection"
        payload_create = {
            "name": mc_name,
            "description": description,
            "user_id": self.user_id
        }

        result_create = requests.post(
            url=url_create,
            headers=self.headers,
            json=payload_create,
        )

        new_media_collection = result_create.json()
        media_collection_id = new_media_collection.get("id")


        self.add_to_mc(deployment_id,  list_image_filename, media_collection_id)

        return media_collection_id

    def add_to_mc(self, deployment_id,  list_image_filename, media_collection_id):
        url_add = \
            self.url + "/api/media_collection/" + str(media_collection_id) \
            + "/media"

        filters = [
            {
                "name": "key",
                "op": "in",
                "val": list_image_filename
            },
            {"name": "deployment_id", "op": "eq", "val": deployment_id},
        ]

        # query should be included in url or payload
        filters_json = json.dumps(
            {"filters": filters})  # , separators=(',', ':'))

        # url_add += "?q=" + filters_json
        payload_add = {
            "user_id": self.user_id,
            "q": filters_json
        }

        result_add = requests.post(
            url=url_add,
            headers=self.headers,
            json=payload_add
        )


    def delete_mc(self, mc_id):
        # DELETE
        # /api/media_collection/<int:id>
        #
        # Delete single media_collection object with id field matching param id
        #
        # Preprocessors:
        # delete_auth_preprocessor(): Assert object delete permission from shared usergroups. Checks current user is owner and object is not shared in a usergroup
        # assert_user_is_owner(): Restrict request to object OWNER only

        url = self.url + '/api/media_collection/' + str(mc_id)
        result = requests.delete(
            url,
            headers=self.headers
        )

        print(result.text)
        return result

    def create_as(self, mc_id, as_name, as_type='point', description=None):
        # POST
        # /api/annotation_set
        #
        # Create new annotation_set object. Expects JSON. Fields shown in MODEL COLUMNS below.
        #
        # Preprocessors:
        # assert_user_login(): Restrict request to logged in users only
        payload_create = {
            "name": as_name,
            "description": description,
            "user_id": self.user_id,
            "media_collection_id": mc_id,
            "tag_scheme_id": 3,
            "data":{
             "type": "pointclick",
             "params": {},
             "allow_add": True,
             "allow_wholeframe": False,
             "labels_per_point": 1,
             "labels_per_frame": 1
            } 
        }
        url = self.url + '/api/annotation_set'
        result = requests.post(
            url,
            headers=self.headers,
            json=payload_create,
        )
        print(result.text)
        new_as = result.json()
        as_id = new_as.get("id")
        return as_id

        pass

    def create_annotation(self, as_id, media_id, x, y, polygon, tag_group_id):
        url_ma = self.url + "/api/media_annotation"
        payload_ma = {
            "annotation_set_id": as_id,
            "media_id": media_id,
            "x": x,
            "y": y,
            "data": {
                "polygon": polygon,
            }
        }
        result = requests.post(
            url_ma,
            headers=self.headers,
            json=payload_ma,
        )
        print(result.text)

        new_media_annotation = result.json()
        media_annotation_id = new_media_annotation.get("id")
        url_a = self.url + "/api/annotation"
        payload_a = {
            "media_annotation_id": media_annotation_id,
            "annotation_set_id": as_id,
            "user_id": self.user_id,
            "tag_group_id": tag_group_id
        }

        result = requests.post(
            url_a,
            headers=self.headers,
            json=payload_a,
        )
        print(result.text)
        return result

    def get_media_collection_media(self, media_collection_id):
        url = self.url + "/api/media_collection/" + str(media_collection_id) + "/media"
        result = requests.get(
            url=url,
            headers=self.headers
        )
        print(result.text)
        return result


if __name__ == '__main__':
    sq_api = SQAPIDirect()

    with open('dive1_D_filelist.txt') as filelist_txt:
        filelist = [x.strip() + " " for x in filelist_txt.readlines()]
    print(filelist)
    # # test
    mc_id = sq_api.create_mc(711, filelist, "AE1_Collection")
    print(mc_id)
    as_id = sq_api.create_as(mc_id, "test")
    # sq_api.create_annotation(as_id, 4915699, 0.5, 0.5, [[-0.05, -0.05], [-0.05, 0.05], [0.05, 0.05], [0.05, -0.05]])
    # sq_api.delete_mc(448)

    # delete
    # for mc_id in range(426, 448):
    #     sq_api.delete_mc(mc_id)