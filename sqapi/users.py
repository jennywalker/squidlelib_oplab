from sqapi.api import SQAPIBase, api_argparser


class SQAPIUser(SQAPIBase):
    user_resource = "users"
    group_resource = "groups"

    def __init__(self, **kwargs):
        SQAPIBase.__init__(self, **kwargs)

    def get_user(self, token=None, id=None, email=None):
        if token is not None:
            filters = [dict(name="api_token", op="eq", val=token)]
            return self.get(resource=self.user_resource, filters=filters,
                            single=True)
        elif id is not None:
            return self.get(resource=self.user_resource, id=id, single=True)
        elif email is not None:
            filters = [dict(name="email", op="eq", val=email)]
            return self.get(resource=self.user_resource, filters=filters,
                            single=True)
        else:
            raise AssertionError(
                "Invalid input arguments. Either 'token' or 'id' or 'email' must be set.")

    def get_user_group(self, id):
        return self.get(resource=self.group_resource, id=id)
