from squidle_direct import SQAPIDirect
import argparse
import json
import numpy as np


# This dictionary is specific to my set of coco classes and the corresponding classes in the VARS system
# For other datasets and other classes, redefine this function
def get_coco_squidle_class_mapping():
    mapping = {
        0: 2262,  # Crab, mapped to
        1: 1239,  # 'Eel'/hagfish, mapped to myxinidae family
        2: 1787,  # Rockfish, mapped to sebastidae family
        3: 1675,  # Flatfish, mapped to
        4: 1023,  # Seastars, mapped to asteroidae family
    }
    return mapping


# assumes a single polygon per coco segmentation
def coco_segmentation_to_squidle_polygon(coco_segmentation, width, height):
    polygon_x = np.array(coco_segmentation[0][0::2])
    polygon_y = np.array(coco_segmentation[0][1::2])
    proportional_polygon_x = polygon_x / width
    proportional_polygon_y = polygon_y / height
    squidle_x = np.mean(proportional_polygon_x)
    squidle_y = np.mean(proportional_polygon_y)
    squidle_polygon_x = proportional_polygon_x - squidle_x
    squidle_polygon_y = proportional_polygon_y - squidle_y
    squidle_polygon = [list(x) for x in list(zip(squidle_polygon_x, squidle_polygon_y))]
    return (squidle_x, squidle_y, squidle_polygon)


def get_coco_squidle_media_mapping(coco_images, squidle_media_collection):
    coco_squidle_mapping = {}
    for image in coco_images:
        filename = image['file_name'].split('/')[-1].split('.')[0]
        coco_id = image['id']
        squidle_id = next(item['id'] for item in squidle_media_collection['objects'] if filename in item["key"])
        coco_squidle_mapping[coco_id] = squidle_id
    return coco_squidle_mapping


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('coco_directory', type=str)
    parser.add_argument('annotation_set_id', type=int)
    parser.add_argument('media_collection_id', type=int)
    args = parser.parse_args()
    coco_directory = args.coco_directory
    annotation_set_id = args.annotation_set_id
    media_collection_id = args.media_collection_id

    sq_api = SQAPIDirect()

    with open(coco_directory) as file:
        coco_json = json.load(file)
    squidle_media_collection = json.loads(sq_api.get_media_collection_media(media_collection_id).text)
    coco_squidle_media_mapping = get_coco_squidle_media_mapping(coco_json['images'], squidle_media_collection)
    coco_squidle_class_mapping = get_coco_squidle_class_mapping()
    for annotation in coco_json['annotations']:
        squidle_x, squidle_y, squidle_polygon = coco_segmentation_to_squidle_polygon(annotation['segmentation'], 2464, 2056)
        sq_api.create_annotation(annotation_set_id, coco_squidle_media_mapping[annotation['image_id']], squidle_x, squidle_y, squidle_polygon, coco_squidle_class_mapping[annotation['category_id']])
    # sq_api = SQAPIDirect()

    # with open('TS1_filelist.txt') as filelist_txt:
    #     filelist = [x.strip()+" " for x in filelist_txt.readlines()]
    # print(filelist)
    # # # test
    # mc_id = sq_api.create_mc(712, filelist, "TS1_Collection")
    # print(mc_id)
    # as_id = sq_api.create_as(mc_id, "test")
    # sq_api.create_annotation(as_id)