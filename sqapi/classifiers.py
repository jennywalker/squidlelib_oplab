import json
import time
import warnings

from datetime import datetime

import requests

from .mediadata import SQAPIMediaData, api_argparser, SQMediaObject

api_argparser.add_argument("--annotation_set_id", type=int, help="Annotation Set ID. If unset, will classify all shared sets.", required=False, default=None)
api_argparser.add_argument("--algname", type=str, help="Algorithm name (default: '{user[first_name]}-{user[last_name]}')", default='{user[first_name]}-{user[last_name]}')
api_argparser.add_argument("--prob_thresh", type=float, help="Probability threshold for positive prediction (default: 0.5)", default=0.5)
api_argparser.add_argument("--poll_delay", type=float, help="Number of seconds delay between checking for shared datasets (default: 10). To run only once, set to '-1'", default=10)


class SQAPIBaseClassifier(SQAPIMediaData):
    def __init__(self, algname="MagicBOT", prob_thresh=0.5, **kwargs):
        SQAPIMediaData.__init__(self, **kwargs)
        self.algname = algname            # initialise with name
        self.prob_thresh = prob_thresh   # set probability threshold
        self.user = self.get_user(token=self.api_token)
        self.classid_translator = None
        self.tag_scheme_id = None
        self.labels = []

    def build_classid_translator(self, tag_scheme_id=None, asid=None, class_labels=None, match_field="name", classid_translator=None):
        # create a lookup / hash of the different classifier labels and how they map to the tag_group_ids
        self.classid_translator = {}
        if classid_translator is not None:
            self.classid_translator = classid_translator
        else:
            # Check that ewe have a tag_scheme_id and get it from annotation_set if not supplied.
            if tag_scheme_id is None and asid is not None:
                tag_scheme_id = self.get_annotation_set(asid).get("tag_scheme", {}).get("id", None)
            assert tag_scheme_id is not None, "Invalid tag_scheme_id. Need to reference a tag_scheme to build the translator."

            class_labels = class_labels or self.labels

            self.classid_translator = {}
            print("Building classification scheme translator for tag_scheme_id={}...".format(tag_scheme_id))
            for l in class_labels:
                print(" * Matching tag_group[{}]: '{}': ".format(match_field, l), end='')
                filters = [{"name":match_field,"op":"ilike","val":"%25{}%25".format(l)},{"name":"tag_scheme_id","op":"eq","val":tag_scheme_id}]
                r = requests.get('%s/api/tag_group?q={"filters":%s,"limit":1,"single":true}' % (self.base_url, json.dumps(filters)))
                if r.status_code == 200:
                    r_json = r.json()
                    self.classid_translator[l] = r_json.get("id")
                    print("MATCH: '{}'".format(r_json.get("name")))
                else:
                    print("NO MATCH FOUND. Ignoring...")

        # check that we have a valid lookup
        assert bool(self.classid_translator), "No classid translator. This is needed to translate classifier classes to SQ+ class IDs"

    def predict(self, media_path, points, **kwargs):
        labeled_points = []
        mediaobj = SQMediaObject(media_path)
        for p in points:
            # check if full frame label or point label
            x, y = p.get("x", None), p.get("y", None)
            if x is not None and y is not None:
                classifier_code, prob = self.predict_point(p, mediaobj)
            else:
                classifier_code, prob = self.predict_whole_frame(p, mediaobj)

            # translate label
            labeled_point = self.get_labeled_point(p, classifier_code, prob)

            # add to labelled list
            if labeled_point is not None:
                labeled_points.append(labeled_point)

        return labeled_points

    def predict_point(self, p, mediaobj, **kwargs):
        """ returns: classifier_code, prob """
        raise NotImplemented("This method needs to be implemented in: '{}'".format(self.__class__.__name__))

    def predict_whole_frame(self, p, mediaobj, **kwargs):
        """ returns: classifier_code, prob """
        warnings.warn("The 'predict_whole_fame' method is not implemented in : '{}'".format(self.__class__.__name__))
        return None, None

    def get_media(self, media_path, **kwargs):
        data = self.url_to_image(media_path)
        data_shape = data.shape
        return {"data": data, "xdim": data_shape[1], "ydim": data_shape[0], "path": media_path}

    def get_labeled_point(self, point, label, prob):
        if label in self.classid_translator:
            # lookup tag_group_id from classification scheme
            tag_group_id = self.classid_translator[label]
            # append to label list of dicts with random probability
            return {
                "tag_group_id": tag_group_id,
                "prob": prob,
                "media_annotation_id": point['id']
            }
        return None

    def submit_predictions(self, labeled_points, annotation_set_id=None, user_id=None):
        assert isinstance(annotation_set_id, int), "Invalid annotation_set_id"
        # if annotation_set_id is None:
        #     annotation_set_id = self.annotation_set.get("id", None)
        if user_id is None:
            user_id = self.user.get("id", None)
        j = 0
        # TODO: Replace this loop with a batch create. Patch /api/media/media_id/annotation_id
        for ma in labeled_points:
            # Create new annotation label
            if ma['prob'] > self.prob_thresh:  # if prob is greater than threshold make a new annotation label
                j += 1  # counter number of point labeled
                annotation = {
                    "annotation_set_id": int(annotation_set_id),
                    "tag_group_id": int(ma['tag_group_id']),  # the tag_group to use for this label
                    "data": {"probability": float(ma['prob'])},
                    "media_annotation_id": int(ma['media_annotation_id']),
                    "user_id": int(user_id)
                }
                #self.new_annotation(annotation)
                self.new_annotation_retry(annotation, 10)
        return j

    def create_derived_annotation_set(self, original_annotation_set_id):
        assert isinstance(original_annotation_set_id, int), "Invalid annotation set ID"
        original_annotation_set = self.get_annotation_set(original_annotation_set_id)
        timestamp = datetime.now().strftime('%Y/%m/%dT%H:%M:%S')
        new_annotation_set_name = self.algname.format(user=self.user)  # + " " + timestamp  # +' [' + original_annotation_set['name'] + ']'

        # self.original_annotation_set_id = original_annotation_set_id
        # self.media_collection_id = original_annotation_set['media_collection_id']
        annotation_set = self.new_annotation_set(
            original_annotation_set.get('media_collection',{}).get('id'),
            new_annotation_set_name,
            original_annotation_set.get('tag_scheme',{}).get("id"),
            self.user['id'],
            parent_annotation_set_id=original_annotation_set["id"],
            description="Suggested annotations by '" + self.algname + "' for the '" + original_annotation_set['name']
                        + "' annotation set. This is not a standalone annotation set."
        )
        print ("New annotation set: '{}', ID={}".format(annotation_set['name'], annotation_set['id']))
        return annotation_set

    def classify_all_shared(self, poll_delay=10, class_labels=None):
        while True:
            print("Checking for unclassified annotation_sets shared with: {}-{}...".format(self.user.get('first_name'), self.user.get('last_name')), end="")
            filters = [
                {'name': "parent_annotation_set_id", 'op': "is_null"},
                {"not": dict(name="children", op="any", val=dict(name="user_id", op="eq", val=self.user.get("id")))}
            ]
            result = self.get(resource=self.annotation_set_resource, validate_api_output=False, filters=filters)
            sets = result.get("objects", [])
            print(" * Found: {} unclassified annotation_sets!".format(len(sets)))
            for i, s in enumerate(sets, start=1):                                          # check sets
                print(" * Classifying: {} ({}/{})...".format(s.get("name"), i, len(sets)))
                # self.build_classid_translator(asid=s.get("id"), class_labels=None)
                new_annotation_set = self.run(s.get("id"), class_labels=None)

            # if negative delay, only run once
            if poll_delay>0:
                time.sleep(poll_delay)
            else:
                break

    def run(self, origin_asid, **translator_kwargs):
        assert isinstance(origin_asid, int) and origin_asid > 0, "Invalid annotation_set ID supplied :{}. Must be int>0".format(origin_asid)
        start_time = time.time()

        # Create a new annotation set
        new_annotation_set = self.create_derived_annotation_set(origin_asid)
        asid = new_annotation_set.get("id")
        media_collection_id = new_annotation_set.get("media_collection",{}).get("id")
        tag_scheme_id = new_annotation_set.get("tag_scheme",{}).get("id")
        if self.classid_translator is None:
            self.build_classid_translator(tag_scheme_id, **translator_kwargs)

        page = 1
        total_pages = None
        media_count = 0
        label_count = 0
        point_count = 0
        pred_count = 0

        # Paginate through results if necessary
        print("Processing media_collection objects...")
        while total_pages is None or page <= total_pages:
            # Get list of media items for selected annotation set
            media_list = self.get_media_list(media_collection_id, page=page)
            page = media_list.get("page")+1
            total_pages = media_list.get("total_pages")
            num_results = media_list.get("num_results")

            # loop through media items and do STUFF
            for m in media_list['objects']:
                media_count += 1
                print("MEDIA ITEM: {}/{}".format(media_count, num_results))

                # Get the annotation label locations from the API (HTTP GET REQUEST)
                get_label_time = time.time()
                media_annotations = self.get_media_annotations(m['id'], origin_asid)
                points = media_annotations['annotations']
                n_points = len(points)
                point_count += n_points
                print(" - Got {} points in {}s...".format(n_points, time.time()-get_label_time))

                # Get labeled annotations from classifier
                prediction_time = time.time()
                labeled_points = self.predict(m['path_best'], points)
                n_pred_labels = len(labeled_points)
                pred_count += n_pred_labels
                print(" - Predicted {}/{} labels in {}s...".format(n_pred_labels, n_points, time.time()-prediction_time))

                # Create a new annotation set
                submit_time = time.time()
                n_saved = self.submit_predictions(labeled_points, annotation_set_id=asid)
                label_count += n_saved
                print(" - Saved {}/{} labels with prob>{} in {}s...".format(n_saved, len(labeled_points), self.prob_thresh, time.time() - submit_time))

        print("\nDONE! Updated {} labels / {} predictions / {} points from {} media items in {}s...".format(
            label_count, pred_count, point_count, media_count, time.time() - start_time)
        )
        print("\nNEW ANNOTATION_SET:\n * name: {}\n * id: {}\n\n".format(
            new_annotation_set.get("name"), new_annotation_set.get("id"))
        )
        return new_annotation_set
