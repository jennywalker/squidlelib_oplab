import requests
import xmltodict
from sqapi.datasource_repositories.datasource import register_datasource_plugin, DataSource, argparser


class AWS(DataSource):
    def list_object_paths(self, url):
        page = requests.get(url).text
        data = xmltodict.parse(page)
        try:
            dirs = [dict(path=i.get('Prefix'),
                         basename=self.get_object_basename(i.get("Prefix")),
                         type="dir",
                         mtime=None,
                         size=None)
                    for i in data.get('ListBucketResult', {}).get('CommonPrefixes', [])]
            files = [dict(path=i.get("Key"),
                          basename=self.get_object_basename(i.get("Key")),
                          type="file",
                          mtime=i.get("LastModified"),
                          size=i.get("Size"))
                     for i in data.get('ListBucketResult', {}).get('Contents', [])]
            return dirs + files
        except Exception as e:
            return []


# Register datasource plugin
register_datasource_plugin(AWS, name="AWSS3")


if __name__ == "__main__":
    args = argparser.parse_args()
    ds = AWS(**vars(args))

    # process all data files
    # We would normally replace input arg with function to process object, but in this case, we're just printing
    ds.process_all_datafiles(func=None)

    # # other examples:
    # # list all campaigns
    # campaigns = ds.list_campaigns()
    # print(campaigns)
    #
    # # list all deployments
    # alldeployments = ds.list_deployments()
    # print(alldeployments)
    #
    # # list deployments from specific campaign
    # deployments = ds.list_deployments(campaign=campaigns[0])
    # print(deployments)
    #
    # # list all data files
    # alldatafiles = ds.get_datafiles()
    # print(alldatafiles)
    #
    # # list datafiles from specific campaign/deployment
    # datafiles = ds.get_datafiles(campaign=campaigns[0], deployment=deployments[0])
    # print(datafiles)

