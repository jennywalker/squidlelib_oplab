import requests
from .datasource import DataSource, argparser, register_datasource_plugin


class GCloud(DataSource):
    def list_object_paths(self, url):
        # print(f"*** PATH: {path}")
        data = requests.get(url).json()

        dirs = [dict(path=i,
                     basename=self.get_object_basename(i),
                     type="dir",
                     mtime=None,
                     size=None)
                for i in data.get("prefixes", [])]
        files = [dict(path=i.get("name"),
                      basename=self.get_object_basename(i.get("name")),
                      type="file",
                      mtime=i.get("updated"),
                      size=i.get("size"))
                 for i in data.get("items", [])]
        return dirs + files


# Register datasource plugin
register_datasource_plugin(GCloud)


if __name__ == "__main__":
    args = argparser.parse_args()
    ds = GCloud(**vars(args))

    # process all data files
    # We would normally replace input arg with function to process object, but in this case, we're just printing
    ds.process_all_datafiles(func=None)

    # # other examples:
    # # list all campaigns
    # campaigns = ds.list_campaigns()
    # print(campaigns)
    #
    # # list all deployments
    # alldeployments = ds.list_deployments()
    # print(alldeployments)
    #
    # # list deployments from specific campaign
    # deployments = ds.list_deployments(campaign=campaigns[0])
    # print(deployments)
    #
    # # list all data files
    # alldatafiles = ds.get_datafiles()
    # print(alldatafiles)
    #
    # # list datafiles from specific campaign/deployment
    # datafiles = ds.get_datafiles(campaign=campaigns[0], deployment=deployments[0])
    # print(datafiles)

