from mediadata import SQAPIMediaData
import requests
import json

def get_user_id(url, api_token):
    user_id= requests.get(
            url + "/api/users",
            params={
                "q": json.dumps(
                    {
                        "filters": [
                            {"name": "api_token", "op": "eq", "val": api_token}
                        ],
                        "single": True,
                    }
                )
            },
        ).json().get("id")
    return user_id

def main():
    url = "https://soi.squidle.org"
    api_token = "a10528442ed2f82b598c69aa02e626addf2b0e6ed52a157fd3136460"
    md = SQAPIMediaData(api_token=api_token, url=url)
    user_id = get_user_id(url, api_token)

    filters = [
        {
            "name": "key",
            "op": "in",
            "val": [
                "PCO_190916090521982683_FC.png ",
                "PCO_190916090846513415_FC.png ",
                "PCO_190916090955695546_FC.png ",
            ],
        },
        {"name": "deployment_id", "op": "eq", "val": 732},
    ]

    md.new_media_collection("test_dy00", user_id, filters, url)


def annotation_set_example():
    url = "http://soi.squidle.org"
    api_token = "a10528442ed2f82b598c69aa02e626addf2b0e6ed52a157fd3136460"
    md = SQAPIMediaData(api_token=api_token, url=url)
    user_id = get_user_id(url, api_token)
    #new_annotation_set(media_collection_id, annotation set name, tag_scheme_id (3 for VARS), user_id)
    for user_number in ["%.2d" % (i+1) for i in range(20)]:
        # md.new_annotation_set(158, "grid_annotation_user_"+user_number, 3, user_id, allow_add=False, params={"nX": 8, "nY": 7, "grid": "show"}, annotation_type="grid")
        md.new_annotation_set(158, "segment_annotation_user_"+user_number, 3, user_id, allow_add=True)



if __name__=="__main__":
    main()