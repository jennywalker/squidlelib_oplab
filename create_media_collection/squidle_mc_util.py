import pandas as pd
import numpy as np
import requests
import json

from sqapi.mediadata import SQAPIMediaData


def cnvt_filename_bc2acfr(filename_biocam):
    # biocam filename example
    # 20190916_174338_930720_20190916_174337_573850_pcoc.png
    # acfr filename example
    # PCO_190916090955695546_FC.png
    assert filename_biocam[0:2] == "20" and filename_biocam[
                                            45:54] == "_pcoc.png", (
            filename_biocam + "is not appropreate biocam filename."
    )

    ret = "PCO_"
    ret += filename_biocam[2:8]
    ret += filename_biocam[9:15]
    ret += filename_biocam[16:22]
    ret += "_FC.png"
    return ret


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i: i + n]


def create_sq_img_filename_filter(
        media_collection_name,
        list_biocam_imgname,
        deployment_id=734,
        url="http://soi.squidle.org",
        api_token="29faefa1b8b7ae436f53f17e731388181c501a11da4f46dfb583220b",
):
    """

    :param media_collection_name: newly created media collection name
    :param list_biocam_imgname: list of biocam image name. (ex. 20190916_174338_930720_20190916_174337_573850_pcoc.png)
    :param deployment_id: deployment ID on squidle. default = 734 (dy109
    cruise)
    :param url: URL of squidle site. default: soi.squidle.org
    :param api_token: api token of squidle. default: Takaki's token
    :return:
    """

    list_acfr_imgname = [None] * len(list_biocam_imgname)
    for idx_imgname, biocam_imgname in enumerate(list_biocam_imgname):
        list_acfr_imgname[idx_imgname] = cnvt_filename_bc2acfr(
            biocam_imgname) + " "

    md = SQAPIMediaData(api_token=api_token, url=url)
    user_id = (
        requests.get(
            url + "/api/users",
            params={
                "q": json.dumps(
                    {
                        "filters": [
                            {"name": "api_token", "op": "eq", "val": api_token}
                        ],
                        "single": True,
                    }
                )
            },
        )
            .json()
            .get("id")
    )
    # filters = [
    #     {
    #         "name": "key",
    #         "op": "in",
    #         "val": [
    #             "PCO_190916090521982683_FC.png ",
    #             "PCO_190916090846513415_FC.png ",
    #             "PCO_190916090955695546_FC.png ",
    #         ],
    #     },
    #     {"name": "deployment_id", "op": "eq", "val": 734},
    # ]

    filters = []
    for batch in chunks(list_acfr_imgname, 100):
        filters.append(
            [
                {"name": "key", "op": "in", "val": batch},
                {"name": "deployment_id", "op": "eq", "val": deployment_id},
            ]
        )

    result = md.new_media_collection(media_collection_name, user_id,
                                     filters)
    return result
