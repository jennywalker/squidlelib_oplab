import glob
import os

import numpy as np
import pandas as pd

from create_media_collection.squidle_mc_util import \
    create_sq_img_filename_filter
from tqdm import trange


def create_mc_tutorial_5_coral():
    rep3_dirpath = \
        '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_selection/representative_patches'
    rep5_dirpath = \
        '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_selection_5/representative_patches'
    idx_coral_cluster = 4

    media_collection_name = 'dy109_tutorial'

    # csv file of clusters other than coral cluster
    list_rep_filepath = glob.glob(
        (rep3_dirpath + '/*/representative.csv'))
    list_rep_filepath.sort()
    del list_rep_filepath[idx_coral_cluster]

    # add filename other than coral cluster
    list_biocam_filename = []
    for rep_csv_filepath in list_rep_filepath:
        df_rep = pd.read_csv(rep_csv_filepath)
        list_biocam_filename.append(
            os.path.basename(df_rep['image file name'][0]))

    # csv file for coral cluster
    rep_coral_filepath = \
        rep5_dirpath + '/cluster_' + str(idx_coral_cluster).zfill(3) \
        + '/representative.csv'
    df_coral = pd.read_csv(rep_coral_filepath)
    # 5 coral image path
    num_coral_img = 5
    for idx_coral in range(num_coral_img):
        list_biocam_filename.append(
            os.path.basename(df_coral['image file name'][idx_coral]))

    for i_img, img_filename in enumerate(list_biocam_filename):
        list_biocam_filename[i_img] = img_filename.replace('_004.png', '.png')

    # swapping (discussed on meeting on 11th May)
    list_biocam_filename.remove(
        '20190916_200531_639979_20190916_200530_338922_pcoc.png')
    list_biocam_filename.append(
        '20190916_165254_803650_20190916_165253_431450_pcoc.png')

    pass
    create_sq_img_filename_filter(media_collection_name, list_biocam_filename)


def create_mc_time_trial_16_coral():
    rep3_dirpath = \
        '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_selection/representative_patches'
    rep5_dirpath = \
        '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_selection_5/representative_patches'
    idx_coral_cluster = 4

    media_collection_name = 'dy109_time_trial'

    # csv file of clusters other than coral cluster
    list_rep_filepath = glob.glob(
        (rep3_dirpath + '/*/representative.csv'))
    list_rep_filepath.sort()
    del list_rep_filepath[idx_coral_cluster]

    # add filename other than coral cluster
    list_biocam_filename = []
    for rep_csv_filepath in list_rep_filepath:
        df_rep = pd.read_csv(rep_csv_filepath)
        for idx_file in range(1, len(df_rep)):
            list_biocam_filename.append(
                os.path.basename(df_rep['image file name'][idx_file]))

    # csv file for coral cluster
    rep_coral_filepath = \
        rep5_dirpath + '/cluster_' + str(idx_coral_cluster).zfill(3) \
        + '/representative.csv'
    df_coral = pd.read_csv(rep_coral_filepath)
    # 16 coral image path
    num_coral_img = 5
    for idx_coral in range(5, 21):
        list_biocam_filename.append(
            os.path.basename(df_coral['image file name'][idx_coral]))

    for i_img, img_filename in enumerate(list_biocam_filename):
        list_biocam_filename[i_img] = img_filename.replace('_004.png', '.png')

    # swapping (discussed on meeting on 11th May)
    list_biocam_filename.remove(
        '20190916_165254_803650_20190916_165253_431450_pcoc.png')
    list_biocam_filename.append(
        '20190916_200531_639979_20190916_200530_338922_pcoc.png')

    pass
    create_sq_img_filename_filter(media_collection_name, list_biocam_filename)


def create_mc_all():
    num_annotator = 20
    remove_tutorial = False
    remove_time_trial = False

    all_img_filepath = '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/clustering_result.csv'
    rep3_dirpath = \
        '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_selection/representative_patches'
    rep5_dirpath = \
        '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_selection_5/representative_patches'
    idx_coral_cluster = 4

    media_collection_name_stem = 'dy109_set_'

    # remove the images in tutorial or time trial
    list_img_to_remove = []
    if remove_tutorial:
        # csv file of clusters other than coral cluster
        list_rep_filepath = glob.glob(
            (rep3_dirpath + '/*/representative.csv'))
        list_rep_filepath.sort()
        del list_rep_filepath[idx_coral_cluster]

        # add filename other than coral cluster
        # list_biocam_filename = []
        for rep_csv_filepath in list_rep_filepath:
            df_rep = pd.read_csv(rep_csv_filepath)
            # list_biocam_filename.append(
            #     os.path.basename(df_rep['image file name'][0]))
            list_img_to_remove.append(
                os.path.basename(df_rep['image file name'][0]))

        # csv file for coral cluster
        rep_coral_filepath = \
            rep5_dirpath + '/cluster_' + str(idx_coral_cluster).zfill(3) \
            + '/representative.csv'
        df_coral = pd.read_csv(rep_coral_filepath)
        # 5 coral image path
        num_coral_img = 5
        for idx_coral in range(num_coral_img):
            # list_biocam_filename.append(
            #     os.path.basename(df_coral['image file name'][idx_coral]))
            list_img_to_remove.append(
                os.path.basename(df_coral['image file name'][idx_coral]))

    if remove_time_trial:
        # csv file of clusters other than coral cluster
        list_rep_filepath = glob.glob(
            (rep3_dirpath + '/*/representative.csv'))
        list_rep_filepath.sort()
        del list_rep_filepath[idx_coral_cluster]

        # add filename other than coral cluster
        # list_biocam_filename = []
        for rep_csv_filepath in list_rep_filepath:
            df_rep = pd.read_csv(rep_csv_filepath)
            for idx_file in range(1, len(df_rep)):
                # list_biocam_filename.append(
                #     os.path.basename(df_rep['image file name'][idx_file]))
                list_img_to_remove.append(
                    os.path.basename(df_rep['image file name'][idx_file]))

        # csv file for coral cluster
        rep_coral_filepath = \
            rep5_dirpath + '/cluster_' + str(idx_coral_cluster).zfill(3) \
            + '/representative.csv'
        df_coral = pd.read_csv(rep_coral_filepath)
        # 16 coral image path
        num_coral_img = 5
        for idx_coral in range(5, 21):
            # list_biocam_filename.append(
            #     os.path.basename(df_coral['image file name'][idx_coral]))
            list_img_to_remove.append(
                os.path.basename(df_coral['image file name'][idx_coral]))

    list_biocam_filename = pd.read_csv(all_img_filepath, index_col=None)[
        'image file name'].values.tolist()
    for idx_filepath in range(len(list_biocam_filename)):
        list_biocam_filename[idx_filepath] = os.path.basename(
            list_biocam_filename[
                idx_filepath])

    for img_to_remove in list_img_to_remove:
        list_biocam_filename.remove(img_to_remove)

    for i_img, img_filename in enumerate(list_biocam_filename):
        list_biocam_filename[i_img] = img_filename.replace('_004.png', '.png')

    for idx_annotator in range(1, num_annotator + 1):
        mc_name = media_collection_name_stem + str(idx_annotator).zfill(2)
        list_cur_annotator_img = []
        for idx_all_img in range(len(list_biocam_filename)):
            if idx_all_img % num_annotator == idx_annotator:
                list_cur_annotator_img.append(
                    list_biocam_filename[idx_all_img])
            else:
                continue
        if idx_annotator < 4:
            create_sq_img_filename_filter(mc_name, list_cur_annotator_img)


def create_mc_from_each_cluster(
        representative_dirpath, media_collection_name, num_sample=1,
        list_idx_target_cluster=None):
    list_csv_filepath = glob.glob(
        (representative_dirpath + '/*/representative.csv'))
    if not list_idx_target_cluster is None:
        list_tmp_csv_filepath = []
        for idx_target_cluster in list_idx_target_cluster:
            for csv_filepath in list_csv_filepath:
                tmp_dirname = os.path.basename(os.path.dirname(csv_filepath))
                if str(idx_target_cluster).zfill(3) in tmp_dirname:
                    list_tmp_csv_filepath.append(csv_filepath)
        list_csv_filepath = list_tmp_csv_filepath
        pass

    list_csv_filepath.sort()

    # create the list of filepath of target images
    list_img_filename = []
    if num_sample == 1:
        tmp_range = range(1)
    elif num_sample == 8:
        tmp_range = range(1, 9)
    elif num_sample == 9:
        tmp_range = range(9)
    for csv_filepath in list_csv_filepath:
        df_rep = pd.read_csv(csv_filepath, index_col=None)

        for idx_row in tmp_range:
            list_img_filename.append(
                os.path.basename(df_rep['image file name'][idx_row]))

    for i_img, img_filename in enumerate(list_img_filename):
        list_img_filename[i_img] = img_filename.replace('_004.png', '.png')

    create_sq_img_filename_filter(media_collection_name, list_img_filename)


def create_mc_tutorial():
    target_dirpath = '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_patches'

    # for one representative image
    num_sample = 1
    media_collection_name = 'single_image_from_11_clusters'
    create_mc_from_each_cluster(
        target_dirpath, media_collection_name, num_sample=num_sample)

    pass


def create_mc_coral_cluster():
    target_dirpath = '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_patches'

    # for one representative image
    num_sample = 9
    list_idx_target_cluster = [4]
    media_collection_name = 'coral_samples'
    create_mc_from_each_cluster(
        target_dirpath, media_collection_name, num_sample=num_sample,
        list_idx_target_cluster=list_idx_target_cluster)

    pass


def create_mc_time_trial():
    target_dirpath = '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_patches'

    # for eight representative images
    num_sample = 8
    media_collection_name = 'eight_image_from_11_clusters'
    create_mc_from_each_cluster(
        target_dirpath, media_collection_name, num_sample=num_sample)

    pass


def create_mc_for_each_annotator():
    # target_dirpath = '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/representative_patches'

    #     create all images divided by 20 other than tutorial and timetrial images
    media_collection_name_stem = 'all_images_except_9_per_cluster'
    num_of_media_collection = 20
    filelist_all_img_filepath = '/home/ty1u18/PycharmProjects/autoencoder/clustering_result/clustering_20200421070939_dy109_03/clustering_20200422105351_BGM_k-20_best/clustering_result.csv'
    list_filelist_excepted_filepath = glob.glob(
        (os.path.dirname(filelist_all_img_filepath) +
         '/representative_patches/*/representative.csv'))
    #     create image name list without representatives
    list_filename_excepted = []
    for excepted_filepath in list_filelist_excepted_filepath:
        df_excepted = pd.read_csv(excepted_filepath)
        list_filename_excepted += df_excepted[
            'image file name'].values.tolist()
    for idx_filename_excepted, filename_excepted in enumerate(
            list_filename_excepted):
        list_filename_excepted[idx_filename_excepted] = os.path.basename(
            filename_excepted).replace('_004.png', '.png')
    #     create list image name one by one
    df_all_image = pd.read_csv(filelist_all_img_filepath, index_col=None)
    list_filename_for_query = []
    for idx_img in trange(len(df_all_image)):
        tmp_filepath = df_all_image['image file name'][idx_img]
        tmp_filename = os.path.basename(
            tmp_filepath).replace('_004.png', '.png')
        if tmp_filename in list_filename_excepted:
            continue
        else:
            list_filename_for_query.append(tmp_filename)

    for idx_division in range(num_of_media_collection):
        mc_name_tmp = \
            media_collection_name_stem + str(idx_division + 1).zfill(3)
        list_idx_tmp = list(
            range(idx_division, len(list_filename_for_query),
                  num_of_media_collection))
        # list_img_tmp = list_filename_for_query[list_idx_tmp]

        list_img_tmp = [list_filename_for_query[i] for i in list_idx_tmp]

        if idx_division == 0:
            create_sq_img_filename_filter(
                mc_name_tmp, list_img_tmp)


if __name__ == '__main__':
    # create_mc_tutorial()
    # create_mc_time_trial()
    # create_mc_for_each_annotator()
    # create_mc_coral_cluster()

    # create_mc_tutorial_5_coral()
    # create_mc_time_trial_16_coral()
    create_mc_all()
    pass
