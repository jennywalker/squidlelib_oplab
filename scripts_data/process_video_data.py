# import argparse
from glob import glob

import pandas as pd
import sys
import os
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from data_modules.video import CVVideo
from data_modules.geotiff import find_depth_from_multiple_geotiffs

# parser = argparse.ArgumentParser(description="Extract frame from video file")
# parser.add_argument('-f', '--filepath', action="store", type=str, help="Full path to Video file")
# parser.add_argument('-l', '--filelist', default=None, action="store", type=str, help="List of frames")
# parser.add_argument('-o', '--save_dir', action="store", type=str, help="Path to output dir")
# parser.add_argument('-t', '--runtime', action="store", type=float, help="Runtime to grab frame")

# Video list source info
vidfilelist = "https://docs.google.com/spreadsheets/d/e/2PACX-1vTuyHEXXz9RHTz1QVoqgGUJFQ06i_DE2W_GeKiC78cEOPAE2hGYaytEKFs-OJoNXsoaVepckBhGRoqJ/pub?gid=1911267194&single=true&output=csv"
filecol = "File"
timecol = "Runtime (seconds)"
framecol = "Image"
depthcol = "Depth (m)"
latcol = "Lat"
loncol = "Lon"
deploymentcol = "Alignment"

# Filter only good values
FILTERS = {
    "IsValid": True
}

# Data locations
frame_name_format = "{r[%s]}/frames/{r[%s]}_{r[%s]:.3f}.jpg" % (deploymentcol, filecol, timecol)
vidfiledir = "/Users/ariell/Documents/data/Cardno/VideoLinks"
#geotiffdir = "/Users/ariell/Google Drive/MyBiz/Greybits Projects/Cardno/Data/geotifs"
geotiffdir = "/Users/ariell/Google Drive/MyBiz/Greybits Projects/Cardno/Data/V190770 NBN Stage 2/Geotiffs"
savedir = "/Users/ariell/Documents/data/Cardno/converted"


# Load geotiff files
gtif_list = glob(os.path.join(geotiffdir, "*.tif"))


# Get frames from video files ##########################################################################################
vid = None
# open CSV file
df = pd.read_csv(vidfilelist)
df[framecol] = df[framecol].astype(str)  # convert to string
print("Loaded data: ({} rows)".format(df.shape[0]))
# Filter rows (if necessary)
for k, v in FILTERS.items():
    df = df[df[k] == v]
    print("Filtered: {}={} ({} rows)".format(k, v, df.shape[0]))

# iterate through rows
for i, r in df.iterrows():
    # check for video file:
    vidfile = os.path.join(vidfiledir, r[filecol])
    if os.path.isfile(vidfile):  # and pd.isna(r[framecol]):
        # print("Capturing frame from: {} @ {:.3f}".format(vidfile, r[timecol]))
        # if vid is None or vid.filepath != vidfile:
        #     vid = CVVideo(vidfile, savedir)
        fname = os.path.join(savedir, frame_name_format.format(r=dict(r)))
        # vid.capture_frame(r[timecol], fname=fname)
        df.at[i, framecol] = os.path.basename(fname)

        # get depths from geotifs
        depth = find_depth_from_multiple_geotiffs(gtif_list, (r[latcol], r[loncol]))
        df.at[i, depthcol] = -depth if depth is not None else None


# Divvy up deployment data
deployments = df[deploymentcol].unique()
for d in deployments:
    df_dpl = df[df[deploymentcol] == d]         # filter other deployments
    df_dpl = df_dpl[df_dpl[framecol] != "nan"]  # filter rows without frames
    df_dpl = df_dpl.replace([np.inf, -np.inf], None)  # replace infs in dataset
    datafile = os.path.join(savedir, "{}".format(d), "navdata.csv")
    if os.path.isdir(os.path.dirname(datafile)):
        df_dpl.to_csv(datafile)
