import json
import os
import sys
from time import time
import requests
import re


sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from sqapi.datasources2 import SQAPIDatasources, api_argparser
from sqapi.mediadata import SQAPIMediaData

DEPLOYMENT_FILEPATTERN = "{base}/{campaign[key]}/Subastian/{deployment[key]}/Squidle_Navlogs/"
EVENTS_FILEPATTERN = "{base}/{campaign[key]}/Subastian/Squidle_Userlogs/events/"
COLLECTION_FILEPATTERN = "{base}/{campaign[key]}/Subastian/Squidle_Userlogs/collections/"
ANNOTATIONSET_FILEPATTERN = "{base}/{campaign[key]}/Subastian/Squidle_Userlogs/annotations/"
USERS_FILEPATTERN = "{base}/{campaign[key]}/Subastian/Squidle_Userlogs/dbdata/users.json"
GROUPS_FILEPATTERN = "{base}/{campaign[key]}/Subastian/Squidle_Userlogs/dbdata/groups.json"
PLATFORMS_FILEPATTERN = "{base}/{campaign[key]}/Subastian/Squidle_Userlogs/dbdata/platforms.json"
SCHEME_FILEPATTERN = "{base}/{campaign[key]}/Subastian/Squidle_Userlogs/annotation_schemes/"


API_ANNOTATIONSETS_URI = '/api/annotation_set?q={"filters":[{"name":"created_at","op":"gt","val":"%s"}]}&results_per_page=1000'
API_MEDIACOLLECTIONS_URI = '/api/media_collection?q={"filters":[{"name":"created_at","op":"gt","val":"%s"}]}&results_per_page=1000'
API_USERS_URI = '/api/users?results_per_page=1000'
API_PLATFORMS_URI = '/api/platform?results_per_page=1000'
API_GROUPS_URI = '/api/groups?results_per_page=1000'
API_TAGSCHEMELIST_URI = '/api/tag_scheme?results_per_page=2000'
API_TAGSCHEME_URI = '/api/tag_group?q={"filters":[{"name":"tag_scheme_id","op":"eq","val":%d}]}&results_per_page=2000'


# parse arguments
api_argparser.add_argument("--export_path", type=str, help="Path to export directory", required=True)
api_argparser.add_argument("--campaign_id", type=int, help="ID of campaign to export (optional)", required=False, default=None)
api_argparser.add_argument("--campaign_key", type=str, help="KEY of campaign to export (optional)", required=False, default=None)
api_argparser.add_argument("--last_campaign", help="Automatically export last campaign", action='store_true', default=False)
api_argparser.add_argument("--filepattern", type=str, help="File path pattern (optional, default: {})".format(DEPLOYMENT_FILEPATTERN), required=False, default=DEPLOYMENT_FILEPATTERN)


def export_campaign_deployments(sqapi_instance, export_path, campaign_id=None, last_campaign=False, campaign_key=None, user_id=None, file_pattern=DEPLOYMENT_FILEPATTERN):
    if campaign_id is not None:
        campaign = sqapi_instance.get_campaign(id=campaign_id)
    elif campaign_key is not None and user_id is not None:
        campaign = sqapi_instance.get_campaign(campaign_key=campaign_key, user_id=user_id)
    elif last_campaign:
        campaign = sqapi_instance.get_last_campaign()
    else:
        campaign = sqapi_instance.get_campaign_ui()

    deployment_list = sqapi_instance.get(resource=sqapi_instance.deployment_resource, results_per_page=1000,
                                         filters=[{"name": "campaign_id", "op": "eq", "val": campaign.get("id")}])

    print ("Found {} deployment matching search...\n - Starting export...".format(deployment_list.get("num_results")))
    n = 0
    for d in deployment_list.get("objects"):
        # if d.get("media_count", 0) > 0:
        tic = time()
        n += 1
        save_path = file_pattern.format(base=export_path, campaign=campaign, deployment=d)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        sqapi_instance.export_deployment(d.get("id"), save_path=save_path, returns=["pose", "posedata", "mediapaths", "datasetkeys"])
        print("Downloaded in {} s...".format(time()-tic))
    return campaign, n


def export_campaign_collections(sqapi_instance, export_path, campaign):
    # get only collections that contain images that come from this deployment
    qfilter = {"name": "media", "op": "any", "val":
        {"name": "deployment", "op": "has", "val":
            {"name": "campaign_id", "op": "eq", "val": campaign.get("id")}}}
    collections = sqapi_instance.get(resource="media_collection", results_per_page=1000, filters=[qfilter])
    for c in collections.get("objects"):
        # Export events
        save_path = EVENTS_FILEPATTERN.format(base=export_path, campaign=campaign)
        print("Exporting {}...".format(save_path))
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        sqapi_instance.export_media_collection(c.get("id"), save_path=save_path, returns=["pose", "posedata", "mediapath", "datasetkeys", "events"], filters=["has-event"])

        # Export Collection
        save_path = COLLECTION_FILEPATTERN.format(base=export_path, campaign=campaign)
        print("Exporting {}...".format(save_path))
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        sqapi_instance.export_media_collection(c.get("id"), save_path=save_path, returns=["pose", "posedata", "mediapath", "datasetkeys"], filters=[])

    # Export Annotations Sets
    # annotation_sets = requests.get(sqapi_instance.base_url + API_ANNOTATIONSETS_URI % (campaign.get("created_at"))).json()
    annotation_sets = sqapi_instance.get(resource="annotation_set", results_per_page=1000, filters=[{"name": "media_collection", "op": "has", "val": qfilter}])
    for a in annotation_sets.get("objects"):
        save_path = ANNOTATIONSET_FILEPATTERN.format(base=export_path, campaign=campaign)
        print("Exporting {}...".format(save_path))
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        sqapi_instance.export_annotation_set(a.get("id"), save_path=save_path, returns=["pose", "posedata", "mediapath", "datasetkeys", "labelinfo", "labelemail", "pointinfo"], filters=["has-label"], transform=None)

    return collections.get("num_results"), annotation_sets.get("num_results")


def export_json_url(url, save_path):
    # Import USERS
    data = requests.get(url).json()
    save_as_json(data, save_path)
    return data.get("num_results")


def save_as_json(data, save_path):
    if not os.path.isdir(os.path.dirname(save_path)):
        os.makedirs(os.path.dirname(save_path))
    with open(save_path, "w") as f:
        f.write(json.dumps(data, indent=4))


def export_annotation_schemes(base_url, path):
    schemes = requests.get(base_url+API_TAGSCHEMELIST_URI).json()
    for s in schemes.get("objects", []):
        save_path = os.path.join(path, make_safe_filename(s.get("name")))
        export_json_url(base_url+API_TAGSCHEME_URI % s.get("id"), os.path.join(save_path, "tag_list.json"))
        save_as_json(s, os.path.join(save_path, "metadata.json"))
        for f in s.get("tag_files",[]):
            r = requests.get(base_url+f.get("fileurl"), allow_redirects=True)
            filename = get_filename_from_cd(r.headers.get('content-disposition'))
            open(os.path.join(save_path,filename), 'wb').write(r.content)

    return schemes.get("num_results")


def get_filename_from_cd(cd):
    """
    Get filename from content-disposition
    """
    if not cd:
        return None
    fname = re.findall('filename=(.+)', cd)
    if len(fname) == 0:
        return None
    return fname[0]


def make_safe_filename(s):
    def safe_char(c):
        if c.isalnum():
            return c
        else:
            return "_"
    return "".join(safe_char(c) for c in s).rstrip("_")


if __name__ == "__main__":
    args = api_argparser.parse_args()
    assert os.path.isdir(args.export_path), "Argument '--export_path' does not appear to be a valid directory !!!"
    start_time = time()
    sqapi_datasources = SQAPIDatasources(api_token=args.api_key, url=args.url)
    sqapi_media = SQAPIMediaData(api_token=args.api_key, url=args.url)
    campaign, ndeployments = export_campaign_deployments(sqapi_datasources, args.export_path, campaign_id=args.campaign_id,
                                                         last_campaign=args.last_campaign, campaign_key=args.campaign_key,
                                                         file_pattern=args.filepattern)
    nannotationschemes = export_annotation_schemes(args.url, SCHEME_FILEPATTERN.format(base=args.export_path, campaign=campaign))
    ncollections, nannotation_sets = export_campaign_collections(sqapi_media, args.export_path, campaign)

    nusers = export_json_url(args.url + API_USERS_URI, USERS_FILEPATTERN.format(base=args.export_path, campaign=campaign))
    ngroups = export_json_url(args.url + API_GROUPS_URI, GROUPS_FILEPATTERN.format(base=args.export_path, campaign=campaign))
    nplatforms = export_json_url(args.url + API_PLATFORMS_URI, PLATFORMS_FILEPATTERN.format(base=args.export_path, campaign=campaign))


    print("\n\nExported {} deployments, {} collections, {} annotation_sets, {} annotation_schemes, {} users, {} groups and {} platforms in {} s...\n\n"
          .format(ndeployments, ncollections, nannotation_sets, nannotationschemes, nusers, ngroups, nplatforms, time() - start_time))