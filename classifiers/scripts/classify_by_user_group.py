import os
import sys
from time import time

import requests

sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
# from classifiers.cnn_keras import KerasClassifier, api_argparser
from classifiers.random_demo import RandomClassifier, api_argparser

api_argparser.add_argument("--group_id", type=int, help="ID of the group", required=True)
api_argparser.add_argument("--export_path", type=str, help="Path to export annotation results", default=None)

args = api_argparser.parse_args()

start_time = time()

# Instantiate classifier
# sq_classifier = KerasClassifier(
#     args.model_path, args.labels_path, args.patch_width, args.patch_height, args.patch_path, args.network,
#     api_token=args.api_key, url=args.url, algname=args.algname, prob_thresh=args.prob_thresh
# )
sq_classifier = RandomClassifier(
    args.media_cache_dir, api_token=args.api_key, url=args.url, algname=args.algname, prob_thresh=args.prob_thresh
)

# get all the users in the group
user_group = sq_classifier.get_user_group(args.group_id)
user_list = user_group.get("users")

# Build up classifier list
errors = []
successful = []
asid_list = []
if args.asid is not None:
    asid_list.append(args.asid)

# Get the annotation_sets from all the users
for u in user_list:
    annotation_sets = requests.get('%s/api/annotation_set?q={"filters":[{"name":"user_id","op":"eq","val":%d}]}&results_per_page=3' % (sq_classifier.base_url, u.get("id"))).json()
    #print ("USER: %s, #annotation_sets: %d" % (u.get("email"), annotation_sets.get("num_results")))
    #asid_list += [a.get("id") for a in annotation_sets.get("objects")]

    for a in annotation_sets.get("objects"):
        print ("http://squidle.org/geodata/collection?media_collection_id=%d&annotation_set_id=%d (%s)" % (a.get("media_collection_id"), a.get("id"), u.get("email")))

    # # DEBUGGING: get fake classification sets
    # for a in annotation_sets.get("objects"):
    #     if bool(a.get("children")):
    #         successful.append({"orig_asid": a.get("id"), "new_asid": a.get("children")[0].get("id")})


exit()

print ("Starting classification on the following ids: {}".format(asid_list))

# Run the classifier
for asid in asid_list:
    try:
        # Build up classifier dictionary in case classification scheme has changed between sets
        sq_classifier.build_classid_translator(asid=asid, info_key="code_short", class_labels=sq_classifier.labels)
        new_annotation_set = sq_classifier.run(asid)
        successful.append({"orig_asid": asid, "new_asid": new_annotation_set.get("id")})
    except Exception as e:
        print("ERROR with asid: %d %s" % (asid, str(e)))
        errors.append({"asid": asid, "error": str(e)})

print ("\n\nDONE! Classified %d/%d annotation_sets with %d errors in %fs..." % (len(successful), len(asid_list), len(errors), time()-start_time))

for e in errors:
    print ("ERROR! ID: %d | %s" % (e.get("asid"), e.get("error")))

# Download validation data
if args.export_path is not None:
    print ("Downloading validation files to: '%s'..." % args.export_path)
    for s in successful:
        sq_classifier.export_annotation_set(s.get("orig_asid"), validation_asid=s.get("new_asid"), save_path=args.export_path)
