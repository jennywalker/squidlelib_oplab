#from io import BytesIO
#from urllib.request import urlopen
from keras.models import load_model
from keras.preprocessing import image
from keras.applications.inception_v3 import InceptionV3, preprocess_input
import os
import cv2
from PIL import Image
import numpy as np
import importlib
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from sqapi.classifiers import SQAPIBaseClassifier, api_argparser

api_argparser.add_argument("--model_path", type=str, help="Path to Keras classifier model", required=True)
api_argparser.add_argument("--labels_path", type=str, help="Path to label file", required=True)
api_argparser.add_argument("--network", type=str, help="Network architecture package (default 'keras.applications.inception_v3')", default="keras.applications.inception_v3")
api_argparser.add_argument("--patch_height", type=int, help="Height of image patch (default 299)", default=299)
api_argparser.add_argument("--patch_width", type=int, help="Width of image patch (default 299)", default=299)
api_argparser.add_argument("--patch_path", type=str, help="Path to cache image patches", required=False, default=None)


class KerasClassifier(SQAPIBaseClassifier):
    def __init__(self, model_path, labels_path, patch_width, patch_height, patch_path, network_package, **kwargs):
        # NB: possible_codes is an additional argument that is only relevant for this model

        SQAPIBaseClassifier.__init__(self, **kwargs)
        #self.preprocess_input = importlib.import_module(network_package, "preprocess_input")
        self.preprocess_input = preprocess_input
        self.model = load_model(model_path)
        self.model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
        self.patchsize = [patch_width,patch_height]
        #f = open(labels_path, 'rb')
        #lines = f.readlines()
        #self.labels = [str(w).replace("\n", "") for w in lines]
        self.labels = ["MALCEK","Other"]
        print ("labels used by classifier {}".format(self.labels))

        self.patch_path = patch_path
        if self.patch_path is not None:
            self.maybe_makedir(self.patch_path)

    # def prep_patch(self, patchPath):
    #     if not os.path.isfile(patchPath):
    #         raise OSError('File does not exist {}'.format(patchPath))
    #
    #     url = 'file:///' + patchPath
    #     fd = urlopen(url)
    #     image_file = BytesIO(fd.read())
    #     img = Image.open(image_file)
    #
    #     x = image.img_to_array(img)
    #
    #     return x

    def maybe_makedir(self, dirname, force=False):
        if not os.path.isdir(dirname) or force:
            os.makedirs(dirname)

    def get_patch(self, x, y, patchsize, mediaobj):
        imagename = os.path.basename(mediaobj.url)    # TODO: improve this. If image names are not unique accross instruments / deployments this will break
        padsize = int((max(patchsize) - 1) / 2)
        cropfile_name = "{}_{}_{}_{}.jpg".format(imagename, x, y, patchsize)
        cropfile_path = os.path.join(self.patch_path, cropfile_name) if self.patch_path else None

        if cropfile_path is not None and os.path.isfile(cropfile_path):
            # return cached image if it exists
            # return self.prep_patch(cropfile_path)
            return image.img_to_array(Image.open(cropfile_path))
        else:
            # convert to padded coordinates

            # check if data has been padded and if not, process it
            if not mediaobj.is_processed:
                orig_image = mediaobj.data()
                image_data = mediaobj.data(cv2.copyMakeBorder(orig_image, padsize, padsize, padsize, padsize, cv2.BORDER_REFLECT_101))
            else:
                image_data = mediaobj.data()   # has already been padded, so will return padded image

            # get patch from image with padding
            x_padded = int(round(x * mediaobj.width) + padsize)
            y_padded = int(round(y * mediaobj.height) + padsize)
            crop_image = image_data[y_padded - padsize: y_padded + padsize + 1, x_padded - padsize: x_padded + padsize + 1]

            # cache image if path is set
            if cropfile_path is not None:
                cv2.imwrite(cropfile_path, crop_image)

            return crop_image

    def predict_point(self, p, mediaobj, **kwargs):
        """ returns: classifier_code, prob """
        x = p.get("x", None)
        y = p.get("y", None)

        patch_img = self.get_patch(x, y, self.patchsize, mediaobj)
        patch_img = self.preprocess_input(patch_img.astype(np.float32))
        predictions = self.model.predict(np.expand_dims(patch_img, axis=0), verbose=1)
        predictions = predictions[0]
        top_k = predictions.argsort()[-3:][::-1]  # prediction in descending probability
        classifier_code = self.labels[top_k[0]]
        prob = predictions[top_k[0]]
        return classifier_code, prob


if __name__ == "__main__":
    # Get arguments from CLI
    args = api_argparser.parse_args()

    # Create classifier object
    sq_classifier = KerasClassifier(
        args.model_path, args.labels_path, args.patch_width, args.patch_height, args.patch_path, args.network,
        api_token=args.api_key, url=args.url, algname=args.algname, prob_thresh=args.prob_thresh
    )

    sq_classifier.build_classid_translator(asid=args.asid, info_key="code_short")
    new_annotation_set = sq_classifier.run(args.asid)
