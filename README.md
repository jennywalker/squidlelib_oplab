# squidlelib #

A for interacting with the SQUIDLE+ API.

### Setup ###

```
#!bash

git clone git@bitbucket.org:ariell/squidlelib.git
cd squidlelib/
virtualenv env            # optional for virtualenv
source env/bin/activate   # optional for virtualenv
pip install -r requirements.txt
```

### Examples ###
#### Run remote annotation script (demo) ####
```
#!python
python examples/demo_annotation_algorithm.py --api_key <str:api_token> --asid <int:annotation_set_id> --url http://203.101.232.29/
```

Where ```<str:api_token>``` is the API TOKEN of the user that will be making requests from the API, and ```<int:annotation_set_id>``` is the annotation_set_id than can be found in the URL of a selected annotation set.

#### Upload data from remote datasource / manage platforms ####
```
#!python
# Sync all datasets as scheduled task forever!
# to be implemented, not yet functioning
screen python scheduled_scripts/sync_platform_datasource_weekly.py --api_key API_KEY

# Upload individual deployments
python -m sqapi.datasources <http://urltoserver> <apitoken>  # Sync selected datasets (with GUI)
```
To add a new platform click the "+NEW PLATFORM" option.

### Example Platform Setups ###

Examples can be found at: `platform_templates/`


## Example data structure ##
The system can support a variety of different data structures hosted on a variety of different online repository architectures (eg: AWS, GCloud, Apache HTTD directory listing, Thredds and even custom APIs).
The main requirements for the hosting of imagery are that:

1. The repository is persistent and stable into the future. I.e. the imagery is linked and we don't want the underlying image data to disappear at any point in the future.
1. The data format for a defined data platform is consistent throughout time. I.e. the system will be set up to ingest a defined data format and it expects that data format to remain the same.

Once a data format and repository have been defined for a platform, then the system can be configured to "set and forget" so that any new datasets that get uploaded to the repository will be automatically imported periodically.

**Example 1**
```
CLOUD_STORAGE_REPOSITORY
└─ PLATFORM_NAME
   └─ CAMPAIGN_NAME
      └─ DEPLOYMENT_NAME
         ├─ images/
         └─ metadata.csv
```
**Example 2**
```
CLOUD_STORAGE_REPOSITORY
└─ CAMPAIGN_NAME
   └─ PLATFORM_NAME
      └─ DEPLOYMENT_NAME
         ├─ images/
         └─ metadata.csv
```
**Example 3**
```
CLOUD_STORAGE_REPOSITORY_FOR_PLATFORM
└─ CAMPAIGN_NAME
   └─ DEPLOYMENT_NAME
      ├─ images/
      └─ metadata.csv
```
**Example 4**
It is also possible to create more complicated pipelines. Eg: get a list of `DEPLOYMENT_NAME` and `CAMPAIGN_NAME` from a geoserver instance and then generate a query using the deployment IDs to get image metadata from a custom API to generate a `metadata.csv` file for import (eg: RLS)

Where

* `PLATFORM_NAME` is the name of the platform (eg: the ROV,AUV, towcam platform name, not used on import, but necessary to keep repository organised if multiple platforms)
* `CAMPAIGN_NAME` is the name of a campaign / cruise (used to define campaign name on import)
* `DEPLOYMENT_NAME` is the name of the deployment or dive (used to define deployment name on import)
* `images/` is a directory containing image files (can be called something else)
* `metadata.csv` is a file containing  position info including references to image files in images/ directory (can be called something else)

## Example `metadata.csv` file: ##
An example of the CSV file. Note: filename and column heading can be different. The format is specified upon setting up the data source as a data source definition.
We can define data transformation operations for the data source to convert the file into the expected format, but it is important that 
whatever format is chosen, remains consistent.

| filename  | latitude | longitude | depth | altitude | timestamp | sensor1 | sensor2 | sensorN |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| ROV_20100625T130726.8888.jpg  | 23.123456  | 12.123456 | 95.0 | 2.0 | 25/06/2010 13:07:26.8888 | 3.4 | 8.3 | 5.6 |
| ROV_20100625T130727.8888.jpg  | 23.234567  | 12.234567 | 95.5 | 2.1 | 25/06/2010 13:07:27.8888 | 3.4 | 8.3 | 5.6 |
| ... | ... | ... | ... | ... | ... | ... | ... | ... |

**Notes:**

* The filename can be different from `metadata.csv`, as long as it is consistent across deployments.
* The metadata file must include a single row for each media item (eg: image)
* The minimum required columns include: `filename`, `latitude`, `longitude`, `timestamp`. Highly recommended columns include `altitude` and `depth`. You can optionally include any number of additional data columns (eg: `sensor1`...`sensorN` above) which can be called anything you like. These can optionally be imported if included in the data source definition.
* Column names do not need to match this example exactly, as they can be mapped during import through the data source definition.
* Column formats:
    * `latitude` and `longitude` should be in signed decimal degrees (i.e.: - for S/W, + for N/E), but can also be converted at time of import 
    from `DDD.ffffff C` or `DDD-MM-SS.ff C`, where `DDD` is degrees, `MM` is minutes, `SS` is seconds, `ff[ffff]` is decimal seconds [degrees] and `C` is a character 
    defining the Cardinal direction (i.e.: N or S for lat / W or E for lon) which can be converted into a sign. Columns can be combined at time of import if split accross cols.
    * `depth` and `altitude` are in metres.
    * `timestamp` should *always be UTC* and should contain date and time (as accutate as possible). Simplest if in single column, but format string can be specified and fields can be merged if date and time are in separate columns at time of import.
    * `filename` must exactly match filenames stored in `images/` directory. This column is used to create the reference to the uploaded image/media item.

----
